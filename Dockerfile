# ---- Base Node ---- #
FROM ubuntu:16.04 AS Base
RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 8842ce5e && \
    echo "deb http://ppa.launchpad.net/bitcoin/bitcoin/ubuntu xenial main" > /etc/apt/sources.list.d/bitcoin.list
RUN apt-get update && \
    apt-get install -y build-essential libtool autotools-dev autoconf libssl-dev git-core libboost-all-dev libdb4.8-dev libdb4.8++-dev libminiupnpc-dev pkg-config sudo && \
    apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# ---- Build Source ---- #
FROM base as build
RUN git clone https://github.com/psionin/smartcoin.git /opt/smartcoin
RUN cd /opt/smartcoin && \
	./autogen.sh && \
    ./configure --without-miniupnpc --disable-tests --without-gui && \
    make install


# ---- Release ---- #
FROM ubuntu:16.04 as release 
RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 8842ce5e && \
    echo "deb http://ppa.launchpad.net/bitcoin/bitcoin/ubuntu xenial main" > /etc/apt/sources.list.d/bitcoin.list
RUN apt-get update && \
    apt-get install -y  libboost-all-dev libdb4.8 libdb4.8++ libminiupnpc-dev && \
    apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
RUN groupadd -r smartcoin && useradd -r -m -g smartcoin smartcoin
RUN mkdir /data
RUN chown smartcoin:smartcoin /data
COPY --from=build /opt/smartcoin/src/smartcoind /usr/local/bin/
COPY --from=build /opt/smartcoin/src/smartcoin-cli /usr/local/bin/
USER smartcoin
VOLUME /data
EXPOSE 58585 58583
CMD ["/usr/local/bin/smartcoind",  "-datadir=/data", "-conf=/data/smartcoin.conf", "-server", "-txindex", "-printtoconsole"]